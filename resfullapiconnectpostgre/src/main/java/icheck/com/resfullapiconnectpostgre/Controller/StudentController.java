package icheck.com.resfullapiconnectpostgre.Controller;

import icheck.com.resfullapiconnectpostgre.entity.Student;
import icheck.com.resfullapiconnectpostgre.exeption.ResourceNotFoundException;
import icheck.com.resfullapiconnectpostgre.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@RestController
@RequestMapping("/api/v1")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("/student")
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }
    @GetMapping("/student/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable(value = "id") int studentId)
            throws ResourceNotFoundException {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + studentId));
        return ResponseEntity.ok().body(student);
    }
    @PostMapping("/studentcreate")
    public Student createStudent(@Valid @RequestBody Student student) {
        Student student1= studentRepository.save(student);
        return student1;
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable(value = "id") int studentId,
                                                  @Valid @RequestBody Student studentdetail) throws ResourceNotFoundException {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + studentId));
       student.setName(studentdetail.getName());
        student.setAge(studentdetail.getAge());
        student.setAddress(studentdetail.getAddress());
        final Student updatedStudent = studentRepository.save(student);
        return ResponseEntity.ok(updatedStudent);
    }
    @DeleteMapping("/delete/{id}")
    public Map<String, Boolean> deleteStudent(@PathVariable(value = "id") int studentId)
            throws ResourceNotFoundException {
        Student student= studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found for this id :: " + studentId));
        studentRepository.delete(student);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
